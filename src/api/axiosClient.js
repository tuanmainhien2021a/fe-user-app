import axios from 'axios'
import queryString from 'query-string'
const axiosClient = axios.create({
  baseURL : "https://doancnpm.azurewebsites.net/",
  // baseURL : "http://192.168.1.51:5000/",

  timeout: 3000,
  headers : {
    'content-type' : "application/json-patch+json"
  },
  // method : 'GET',
  // paramsSerializer : params => {queryString.stringify(params);console.log(queryString.stringify(params))},
})
  axiosClient.interceptors.request.use(async (config)=>{
    return config;
  })

  axiosClient.interceptors.response.use((response)=>{
    if (response && response.data){
      return response.data
    }
    return response;
  },err=>{
    // console.log("Lỗi",err);
    return Promise.reject(err.response);
  })

  export default axiosClient;

