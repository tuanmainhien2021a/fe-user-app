const time2 = {
    convertDateandTimeISOString : function(date1,date2){
        date1.setHours(date2.getHours(),date2.getMinutes());
        return date1.toISOString();
    },

    convertDateToDate : function(date){
        const thuVi = ["Thứ 2","Thứ 3","Thứ 4","Thứ 5","Thứ 6","Thứ 7", "Chủ nhật"]
        const thuEn = ["Mon","Tue","Wed","Thu","Fri","Sat", "Sun"];
        const thangVi =  ["01","02","03","04","05","06","07","08","09","10","11","12",];
        const thangEn = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

        const date1 = date.toDateString().split(" ");
        // console.log(date1);

        date1[0] = thuVi  [thuEn.indexOf(date1[0])];
        date1[1] = thangVi[thangEn.indexOf(date1[1])];
        return [date1[0],date1[2]+"/"+date1[1]+"/"+date1[3]];
    },

    convertDateToTime : function(date){
        return date.toTimeString().substr(0,5);
    },

    IsConflictTime : function(list,myDeparture,myLanding){
        console.log(123);
        console.log(new Date(myDeparture).toString());
        console.log(new Date(myLanding).toString());
        let departureIndex = -1;
        let landingIndex = -1;
        const [list1,list2] = list;
        list1.map(value=>{
            console.log(new Date(value).toString());
        })
        list1.forEach((value,index)=>{
          if (myDeparture > value)
            departureIndex = index;  
        })
        list2.forEach((value,index)=>{
          if (myLanding > value)
            landingIndex = index;  
        })
        if (departureIndex ==landingIndex)
            return false;
        return true;

    }


}

export default time2;