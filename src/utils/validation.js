const validation = {
    isEmail : email =>{
        if (email.indexOf("@")<0 || email.indexOf(".")<0)
            return false;
        return true;
    },

    isPassword : password =>{
        const UpperPass = password.toUpperCase();
        const LowerPass = password.toLowerCase();
        if (password ==UpperPass || password ==LowerPass || password.length<8) 
            return false;
        return true;
    },

    isComfirmPassword : (old,newPass) =>{
        return old==newPass
    }
}
export default validation;