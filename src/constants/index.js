export const EmailErrMsg = "Email không hợp lệ"
export const PasswordErrMsg = "Mật khẩu phải bao gồm ít nhất 1 chữ cái in hoa, 1 chữ cái thường và lớn hơn 8 kí tự"
export const ConfirmPasswordErrMsg = "Mật khẩu không trùng khớp"