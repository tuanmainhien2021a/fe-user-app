import AsyncStorage from '@react-native-async-storage/async-storage';


const storage = {
    write : async (key,value) => {
        try {
          await AsyncStorage.setItem(key, value);
        } catch (e) {
          // saving error
        }
      },
    
    
     read : async (key="userToken") => {
        try {
        const value = await AsyncStorage.getItem(key);
        let result ="";
        if(value !== null) {
            // console.log("VALUEtoken99",value);
            result = value;
        }
        // console.log("RESULT=",result);

        return result;
        } catch(e) {
            console.log(e);
        }
    },

    remove : async (key) => {
        try {
          await AsyncStorage.removeItem(key);
          console.log("Deleted" + key +" storage succesful");

        } catch(e) {
          // remove error
        }
      
      },


    
}

export default storage;