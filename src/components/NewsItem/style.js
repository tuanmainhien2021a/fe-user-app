import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
    categoryImage : {
        alignSelf : 'stretch',
        height : 150,
    },
    container : {
        // alignItems : 'center',
        borderRadius : 4,
        paddingBottom : 20,
        shadowColor : "#000",
        shadowOpacity : 0.3,
        shadowRadius : 10,
        shadowOffset : {width :0,height:0},
        marginBottom : 16,
        borderBottomWidth : 1,
        // backgroundColor : '',
    },
    title : {
        fontSize : 35,
        alignItems : 'flex-start'
    },

    shortContent : {
        fontSize : 20,
    }

})
