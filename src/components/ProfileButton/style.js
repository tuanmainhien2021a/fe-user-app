import { StyleSheet } from "react-native";
import _styles from '../../styles/style'
const image = 32;
export default styles = StyleSheet.create({
    categoryImage : {
        alignSelf : 'stretch',
        height : 150,
    },
    container : {
        // alignItems : 'center',
        borderRadius : 4,
        shadowColor : "#000",
        paddingVertical : 15,
        shadowOpacity : 0.3,
        shadowRadius : 10,
        shadowOffset : {width :0,height:0},
        borderBottomWidth : 1,
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'space-evenly',
        backgroundColor : 'white',
    },
    title : {
        fontSize : 20,
        alignItems : 'flex-start',
        fontWeight : 'bold',
    },

    shortContent : {
        fontSize : 20,
    },

    imageContainer : {
        width : 40,
        height : 40,
    },

    col1 : {
        flex : 2,
        alignItems : 'center'
    },

    col2 : {
        flex : 7,
    },

    col3 : {
        flex : 1,
        alignItems : 'center',

    },

    icon : {
        width : image,
        height: image,
        opacity : 0.5,
    }

})