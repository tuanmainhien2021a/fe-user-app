import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import styles  from './styles';

export default class Logo extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <View style= {styles.container}>
                <Image style={styles.imageContainer} source= {require('../../assets/covid1.png')}></Image>
                <View style = {styles.container}>
                    <Text style = {styles.title}>Covid App</Text>
                </View>
            </View>
        )
    }
}
