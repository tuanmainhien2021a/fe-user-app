import React, { useEffect, useState } from 'react';
import { TouchableOpacity } from 'react-native';
import { Alert } from 'react-native';
import { Animated, Text, TextInput, View,Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
const Input = (props) =>{
    // const [isErr,setIsErr] = useState(false);
    const [isFocus,setIsFocus] = useState(false);
    const {oldPass,errMsg,validation,validation1,isErr,multiline,label,value,onChangeText,keyboardType,secureTextEntry,placeholder,iconName,editable} = props
    const [secureText,setSecureText] = useState(secureTextEntry);
    const handleChange = (text)=>{
        onChangeText(text);
        // if (oldPass){
        //     if (validation(oldPass,text))
        //         setIsErr(false)
        //     else
        //         setIsErr(true);
        // }
        // else
        // if (validation(text))
        //     setIsErr(false);
        //  else 
        //      setIsErr(true);
        //  console.log(text,validation(text));
        console.log(isErr);
        
    }
    handleEdit = ()=>{

    }
    function EyePassword() {
        if(secureText){
            return (
                <TouchableOpacity
                 onPress = {()=>setSecureText(false)}
             >
                <Image
                    source = {require('../../assets/eye.png')}
                    style = {styles.eye}
                />
            </TouchableOpacity>
            )
        }
        else 
            if (secureText==false){
                return (
                    <TouchableOpacity
                      onPress = {()=>setSecureText(true)}
                     >
                        <Image
                            source = {require('../../assets/uneye.png')}
                            style = {styles.eye}
                        />
                    </TouchableOpacity>
                )
            }
        return (
            <View>

            </View>
        )
    }
    
    return (

    <View style = {styles.containerInput}>
        <Text style = {styles.label}>{label}</Text>
        <View style={styles.searchSection}>
            <Icon style={styles.icon} name={iconName} size={20} color="#4dd0f7"/>
            <TextInput
                editable = {editable}
                defaultValue = {value}
                style={styles.input}
                placeholder={placeholder || `Nhập ${label}`}
                onChangeText = {handleChange}   
                keyboardType = {keyboardType}
                secureTextEntry = {secureText}
                blurOnSubmit = {false}
                multiline ={multiline}
                onEndEditing= {e=>console.log(e.nativeEvent.text)}
                // returnKeyType = "next"
                onFocus = {e=>{
                   setIsFocus(true);
                }}
                
                
            />

            <EyePassword/>
        </View>

            { isErr && isFocus &&<Text style = {styles.errMsg} >{errMsg}</Text>}
    </View>
    )
}
Input.defaultProps = {
    label : "Label",
    editable : true,
    multiline : false,
    validation : ()=>{},
    errMsg : "",
}

export default Input;