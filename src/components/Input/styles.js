import {StyleSheet} from 'react-native'
const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : 'white',
    },

    searchSection: {
        display : 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth : 2,
        borderRadius : 20,

    },
    input: {
        flex: 1,
        color: '#424242',
        fontWeight : 'bold',
    },
    containerInput : {
        paddingVertical : 10,
        marginHorizontal : 10,
    },
    icon :{
        paddingHorizontal : 15
    },
    label : {
        fontWeight : 'bold',
        paddingBottom : 5,
        paddingLeft : 10,
    },
    title : {
        fontWeight : 'bold',
        fontSize : 30,
        textAlign : 'center',
        backgroundColor : '#2d65ad',
        borderRadius : 20,
        width : 250,
        marginTop : 20,
    },

    errMsg : {
        color : 'red',
        
    },

    eye : {
        width : 30,
        height : 30,
        marginRight : 20,
    }
})

export default styles;