import {StyleSheet} from 'react-native'
import _styles from '../../styles/style'
export default  styles = StyleSheet.create({
    container : {
        justifyContent : 'space-between',
        borderWidth : 0.5,
        borderRadius : 10,
        marginBottom : 20,
        padding : 10,
        // background color
        backgroundColor : 'white',
        flex : 1,

    },

    title : {
        fontSize : 18,
        fontWeight : 'bold',

    },

    data :{
        fontSize : 18,
    },

    row :{
        flexDirection : 'row',
    },

    col : {
        display : 'flex',
        flex : 3,
        backgroundColor : 'yellow'
    },

    imgContainer: {
        width : 50,
        height : 50,
    },
    
    row1 : {
        flex:4
    },
    row2 : {
        flex:7
    },

    result : {
        color : 'red',
        fontSize : 23,

    },

    image : {
        width : 32,
        height : 32,
    },

    header :{
        textAlign : 'center',
        fontWeight : 'bold',
    },

    headerBackground: {
        ..._styles.backgroundBorder
    }
})