import React from 'react';
import { Text, View,TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from "prop-types";


const Button = (props) =>{
    const {title,onPress,titleStyle,containerStyle} = props

    return (
        <View style = {styles.container}>
            <TouchableOpacity
                activeOpacity = {0.3}
                onPress = {onPress}
            >
            <Text style = {{...styles.title,...titleStyle}}>{title}</Text>

            </TouchableOpacity>
        </View>
    )
}
Button.defaultProps = {
    title : "",
    onPress : null,
    titleStyle : {},
    containerStyle : {},

}
export default Button;