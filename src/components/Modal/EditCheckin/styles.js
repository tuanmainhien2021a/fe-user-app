import {StyleSheet} from 'react-native'
import _styles from '../../../styles/style'
const styles = StyleSheet.create({
    container : {
        // backgroundColor : '#e1e6ed',
        backgroundColor : 'white',
        // justifyContent : 'center',
        // alignItems : 'center',
        borderRadius : 20,
        borderWidth : 2,
        // flex : 1,
    },

    header : {
        ..._styles.header
    },

    title : {
        // fontSize : 16,
        fontWeight : 'bold'
    },

    timeContainer  : {
        paddingHorizontal : 15,
    },

    groupButtons  : {
        flexDirection : 'row',
        justifyContent : 'space-around',
        marginVertical : 20,
    },
    submitButton : {
        ..._styles.colorButton,
        paddingHorizontal : 10,

    },

    cancelButton  :{
        ..._styles.cancelButton,
        paddingHorizontal : 10,
    },

    datetimeContainer : {
        ..._styles.borderPicker,
        borderRadius : 20,
        borderWidth : 2,
        // marginHorizontal : 10,
        flexDirection : 'row',
        paddingVertical : 10,
        alignItems : 'center',
        justifyContent : 'space-around'
    },

    row : {
        flexDirection : 'row'
    }


    
});


export default styles;