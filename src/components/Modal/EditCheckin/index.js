import React, {useState} from 'react';
import {Text, View,TextInput, Alert,SafeAreaView,TouchableOpacity} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import Toast from 'react-native-simple-toast';

import styles from './styles';
import Input from '../../Input';
import Button from '../../Button'
import loginApi from '../../../api/loginApi'
import CustomInput from '../../Input'
import AsyncStorage from '../../../storage';
import userApi from '../../../api/userApi';
import time2 from '../../../utils/time2';
export default function (props) {
  const {isShow,setIsShow,item,setLoading,mustUpdate,setMustUpdate} = props;

  const [address,setAddress] = useState (item.address);
  const [time,setTime] = useState (new Date(item.time));
  const [date,setDate] = useState (new Date(item.time));
  const [showDate,setShowDate] = useState(false)
  const [showTime,setShowTime] = useState(false)



  function handleSubmit(){
      setLoading(true);
      AsyncStorage.read()
        .then(token=>{
          const data = {
            address,
            // time : date.toISOString().substr(0,10)+ "T"+ time.toTimeString().substr(0,8)
            time : time2.convertDateandTimeISOString(date,time)
          }
          // console.log("DATA",data);
          return userApi.EditCheckIn(item.id,data,token);
        })
        .then(res=>{
          setMustUpdate(!mustUpdate);
          Toast.show("Chỉnh sửa thành công",Toast.SHORT);
        })
        .catch(err=>{
          console.log(err);
          Toast.show("Có lỗi xảy ra",Toast.SHORT);
        })
        .finally(()=>{
          setLoading(false);
          setIsShow(false);
        })
  }

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowDate(false);
    setDate(currentDate);
  };
  const onChangeTime = (event, selectedTime) => {
    const currentTime = selectedTime || time;
    setShowTime(false);
    setTime(currentTime);
  };

  return (
    <View >
      <Modal isVisible={isShow}>
        <View style={styles.container}>
            <Text style = {styles.header}>
              Chỉnh sửa thông tin Check-in
            </Text>
            <Input style = {styles.input}
              label = "Địa chỉ"
              onChangeText = {setAddress}
              value = {item.address}
              multiline = {true}
            />
            <View style = {styles.timeContainer}>
            <Text style= {styles.title}>Thời gian khởi hành</Text>
                <View style = {styles.datetimeContainer}>

                    {showDate && 
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={date}
                            mode={'date'}
                            is24Hour={true}
                            onChange={onChangeDate}
                        />
                    }

                    <TouchableOpacity 
                        onPress = {()=>{
                            setShowDate(true);
                        }}
                    >
                            <View style = {styles.row}>
                                <Text  style= {styles.datetime}>
                                    {date.toISOString().substr(0,10)}
                                </Text>
                                <Icon name="calendar" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                            </View>
                    </TouchableOpacity>


                    {showTime && 
                        <DateTimePicker
                            testID="dateTimePicker1"
                            value={time}
                            mode= "time"
                            is24Hour={true}
                            onChange={onChangeTime}
                        />
                    }
                    <TouchableOpacity 
                        onPress = {()=>{
                            setShowTime(true);
                        }}
                    >
                        <View style = {styles.row}>
                            <Text  style= {styles.datetime}>
                                {time.toTimeString().substr(0,8)}
                            </Text>
                            <Icon name="time" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                        </View>
                    </TouchableOpacity>
            </View>
                        
            </View>
            <View style = {styles.groupButtons}>
                <Button
                  onPress = {handleSubmit}
                  title = {"Chỉnh sửa"}
                  titleStyle = {styles.submitButton}
                />
                <Button
                  onPress = {()=>{
                      setIsShow(false);
                  }}
                  title = {"Quay lại"}
                  titleStyle = {styles.cancelButton}
                />
            </View>
        </View>
      </Modal>
    </View>
  );
}

