import React, {useState,useRef,useImperativeHandle} from 'react';
import {Button, Text, View,TextInput, Alert,SafeAreaView,TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles'
import Input from '../../Input'
import { forwardRef } from 'react';
import loginApi from '../../../api/loginApi'
function ModalTester(props,ref) {
  const [isModalVisible, setModalVisible] = useState(false);
  const [confirmCode,setConfirmCode] = useState("");
  const {visible} = {props}
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  }
  const email = props.email;
  const password = props.password;

  useImperativeHandle(ref,()=>({
    toggleModal : ()=>{
     setModalVisible(!isModalVisible);
    }
  }))
  const confirmOnClick = () =>{
      console.log(confirmCode);
      loginApi.verifyEmail({email,code : confirmCode})
      .then(res=>{
        //success
        console.log(res);
        if (res.isSuccess){
          Alert.alert("Xác thực email thành công");
          setModalVisible(false);
        }
        else {
            Alert.alert("Mã xác thực chưa đúng");
           
        }
      })
      .catch(err=>{       
        console.log(err);

      })
  }

  return (
    
    <View style={{display:'flex',justifyContent : 'center',alignItems : 'center',backgroundColor : 'red'}}>
      <Modal isVisible={isModalVisible}>
        <View style={styles.container}>
            <SafeAreaView>
                <Text style = {styles.title}>Mã xác nhận đã được gửi qua email của bạn.</Text>
                <Text style={styles.title}>Mã xác nhận:</Text>
                <TextInput style= {styles.input}
                    onChangeText = {setConfirmCode} 
                    placeholder = {"Mã xác nhận"}
                    autoFocus = {true}

                />
            </SafeAreaView>
            <View style = {styles.groupButton}>
                <TouchableOpacity
                    onPress = {confirmOnClick}
                >
                    <Text style = {{...styles.button,...styles.confirm}}>
                        Xác nhận
                    </Text>
               </TouchableOpacity>

                <TouchableOpacity
                    onPress = {toggleModal}
                >
                    <Text style = {{...styles.button,...styles.cancel}}>
                        Hủy bỏ
                    </Text>
               </TouchableOpacity>

            </View>
        </View>
      </Modal>
    </View>
  );
}
ModalTester  = forwardRef(ModalTester)

export default ModalTester;