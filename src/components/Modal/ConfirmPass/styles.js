import {StyleSheet} from 'react-native'
import _styles from '../../../styles/style';

const styles = StyleSheet.create({
    container : {
        // backgroundColor : '#e1e6ed',
        backgroundColor : 'white',
        justifyContent : 'center'
        ,alignItems : 'center',
        height : 350,
        borderRadius : 50,
        borderWidth : 2,
    },

    title : {
        fontWeight : '900',
        fontSize : 30,
        textAlign : 'center'
    },

    input : {
        marginVertical : 20,
        // paddingLeft : 20,
        borderWidth : 2,
        borderRadius : 5,
        // display : 'flex',
        // width : 200,
        paddingHorizontal : 20,
        // justifyContent : 'center'
    },

    groupButton : {
        paddingVertical : 20,
        display : 'flex',
        flexDirection : 'row',
        width : 250,
        justifyContent :'space-between'
        // justifyContent :'flex-start'
    },
    
    button : {
        fontSize : 20,
        fontWeight : 'bold',
        paddingVertical : 10,
        minWidth : 115,
        textAlign  : 'center',
    },

    confirm :{
        backgroundColor : "#10e04e"
    },

    cancel : {
        ..._styles.cancelButton,
    }
    
});


export default styles;