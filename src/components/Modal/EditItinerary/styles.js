import {StyleSheet} from 'react-native'
import _styles from '../../../styles/style'
const styles = StyleSheet.create({
    container : {
        // backgroundColor : '#e1e6ed',
        backgroundColor : 'white',
        // justifyContent : 'center',
        // alignItems : 'center',
        borderRadius : 20,
        borderWidth : 1,
        // flex : 1,
    },

    header : {
        ..._styles.header
    },

    title : {
        // fontSize : 16,
        fontWeight : 'bold'
    },

    timeContainer  : {
        paddingHorizontal : 15,
    },

    groupButtons  : {
        flexDirection : 'row',
        justifyContent : 'space-around',
        marginVertical : 20,
    },
    submitButton : {
        ..._styles.colorButton,
        paddingHorizontal : 10,

    },

    cancelButton  :{
        ..._styles.cancelButton,
        paddingHorizontal : 10,
    },

    datetimeContainer : {
        // ..._styles.borderPicker,
        borderRadius : 20,
        borderWidth : 2,
        // marginHorizontal : 10,
        flexDirection : 'row',
        paddingVertical : 10,
        alignItems : 'center',
        justifyContent : 'space-around'
    },

    row : {
        flexDirection : 'row'
    },

    header : {
        fontSize : 30,
        fontWeight : 'bold',
        textAlign : 'center',
        paddingVertical : 20,
    },

    // row : {
    //     flexDirection : 'row',
    //     alignItems  : 'center',
    // },

    title : {
        fontWeight : 'bold',
        fontSize : 18,
    },

    inputContainer :{
        // borderWidth : 1,
        // borderRadius  : 20,
        // marginHorizontal : 10, 
        // marginVertical : 20,
        // paddingHorizontal : 20,
        paddingVertical : 10,
        paddingHorizontal : 15,
    },

    label : {
        fontSize : 20,
        fontWeight : 'bold',
    },

    titleStyle : {
        ..._styles.colorButton,
        paddingHorizontal : 30,
        marginVertical : 20,
    },

    borderPicker: {
        ..._styles.borderPicker,
        marginHorizontal : 10,
    },

    datetimeContainer : {
        ..._styles.borderPicker,
        marginHorizontal : 10,
        flexDirection : 'row'
        ,paddingVertical : 10,
        alignItems : 'center',
        justifyContent : 'space-around'
    },
    
    textInput: {
        paddingHorizontal : 15
    },

    disableDate : {
        backgroundColor : 'grey',
        color: 'black',
    }


    
});


export default styles;