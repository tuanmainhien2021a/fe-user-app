import DateTimePicker from '@react-native-community/datetimepicker';
import { Picker } from '@react-native-picker/picker';
import React, { useEffect, useState } from 'react';
import { Alert, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/Ionicons';
import cityApi from '../../../api/cityApi';
import userApi from '../../../api/userApi';
import AsyncStorage from '../../../storage';
import time2 from '../../../utils/time2';
import Button from '../../Button';
import styles from './styles';
import MustTestingModal from '../../Modal/MustTesting'

export default function (props) {
  const {listTime,isShow,setIsShow,item,setLoading,mustUpdate,setMustUpdate,tabBarNavigation} = props;
    // console.log("XXX",new Date(item.departureTime).toUTCString());

    // console.log("ITEM From modal",listTime);

    const [isShowModal,setIsShowModal] = useState(false);
    const [listCity,setListCity] = useState([]);
    
    const [departureCityId,setdepartureCityId] = useState(item.departureCityId);
    const [destinationCityId,setDestinationCityId] = useState(item.destinationCityId);
    const [flyNo,setFlyNo] = useState(item.travelNo);

    const [departureDate,setDepartureDate] = useState(new Date(Date.now()));
    const [departureTime,setDepartureTime] = useState(new Date(Date.now()));

    const [landingDate,setLandingDate] = useState(new Date(item.landingTime));
    const [landingTime,setLandingTime] = useState(new Date(item.landingTime));

    const [showDepartureDate,setShowDepartureDate] = useState(false);
    const [showDepartureTime,setShowDepartureTime] = useState(false);

    const [showDestinationDate,setShowDestinationDate] = useState(false);
    const [showDestinationTime,setShowDestinationTime] = useState(false);

    const myDepartureDate = time2.convertDateToDate(departureDate);
    const myLandingDate = time2.convertDateToDate(landingDate);
    //modal

    //component did mount

    useEffect(()=>{
        AsyncStorage.read()
            .then((token)=>{
                return cityApi.getCities(token); 
            })
            .then(res=>{
                const {data} = res;
                setListCity(data);
                // console.log("DANH SACH THANH PHO :",listCity);
                setDepartureDate(new Date(item.departureTime));
                setDepartureTime(new Date(item.departureTime));
            })
            .catch(err=>{
                console.log(err);
            })
    },[])

    function handleSubmit(){
        // if (time2.IsConflictTime(listTime,new Date(time2.convertDateandTimeISOString(departureDate,departureTime))
        //     .getTime(),
        //     new Date(time2.convertDateandTimeISOString(landingDate,landingTime)).getTime()))
        //     {
        //     Alert.alert("Thời gian xung đột với các lịch trình trước đó, vui lòng chọn lại");
        //     return;
        // }
        setLoading(true);
        AsyncStorage.read()
            .then(token=>{
                const data = {
                    departureCityId,
                    destinationCityId,
                    flyNo,
                    // departureTime : departureDate.toISOString().substr(0,10)+"T"+departureTime.toISOString().substr(11,8),
                    departureTime : time2.convertDateandTimeISOString(departureDate,departureTime),
                    // landingTime : landingDate.toISOString().substr(0,10)+"T"+landingTime.toISOString().substr(11,8),
                    landingTime : time2.convertDateandTimeISOString(landingDate,landingTime)


                }
                if (data.departureTime>=data.landingTime){
                    Toast.show("Thời gian không phù hợp, vui lòng thử lại",Toast.LONG);
                    return;
                }
                // console.log("PARAMS",data,item.id);
                return userApi.EditItinerary(item.id,data,token);

            })
            .then(res=>{
                console.log("RES99",res);
                if(res.isSuccess){
                    const {data} = res;
                    if (data.mustTesting){
                        setIsShowModal(true);
                        return;
                    }
                    console.log(999999999999);
                    Toast.show("Chỉnh sửa lịch trình thành công",Toast.SHORT);
                    // setIsShow(false)
                    setMustUpdate(!mustUpdate)
                }
            })
            .catch(err=>{
                const {data} = err;
                console.log("LỖI",data);
                if(!data.isSuccess){
                    Toast.show("Thông tin lịch trình không hợp lệ",Toast.LONG);
                }
            })
            .finally(()=>{
                setLoading(false);
                setIsShow(false)
            })
  }


    const onChangeDepartureDate = (event, selectedDate) => {
        const currentDate = selectedDate || departureDate;
        setShowDepartureDate(false);
        setDepartureDate(currentDate);
      };

    const onChangeDepartureTime = (event, selectedDate) => {
        const currentDate = selectedDate || departureTime;
        setShowDepartureTime(false);
        setDepartureTime(currentDate);
      };

    const onChangeLandingDate = (event, selectedDate) => {
        const currentDate = selectedDate || landingDate;
        setShowDestinationDate(false);
        setLandingDate(currentDate);
      };
    const onChangeLandingTime = (event, selectedDate) => {
        const currentDate = selectedDate || landingTime;
        setShowDestinationTime(false);
        setLandingTime(currentDate);
      };


       

  return (
    <View >
    <MustTestingModal
        isShow = {isShowModal}
        setIsShow = {setIsShowModal}
        navigation = {tabBarNavigation}
    />
      <Modal isVisible={isShow}>
        <View style={styles.container}>

        <ScrollView>

      <View style= {styles.container}>

          <Text style= {styles.header}> Lịch trình di chuyển </Text>

          <View style = {styles.inputContainer}>
              <Text style= {styles.label}>Nơi khởi hành</Text>
              <View style = {styles.borderPicker}>
                  <Picker
                          selectedValue={departureCityId}
                          mode = {'dialog'}
                          onValueChange={(itemValue, itemIndex) =>
                              setdepartureCityId(itemValue)
                          }>
                          {listCity.map((value,index)=>{
                              const enabled = value.id != destinationCityId;
                              // console.log(departureCityId,index,enabled);

                              return (
                                  <Picker.Item 
                                      label ={value.name} 
                                      value = {value.id} 
                                      key={value.id}
                                      enabled = {enabled}
                                  />
                                  )
                          })}
                  </Picker>
              </View>
          </View>

          <View style = {styles.inputContainer}>
              <Text style= {styles.label}>Nơi đến</Text>
              <View style ={styles.borderPicker}>
                  <Picker
                          selectedValue={destinationCityId}
                          mode = {'dialog'}
                          onValueChange={ itemValue =>
                              setDestinationCityId(itemValue)
                          }
                          
                          >
                          {
                          listCity.map((value,index)=>{
                              const enabled = departureCityId != value.id
                              return (
                                  <Picker.Item 
                                      label ={value.name} 
                                      value = {value.id} 
                                      key={value.id}
                                      enabled = {enabled}
                                      
                                  />)
                          })}
                  </Picker>
              </View>
          </View>
{/* 
          <Input
              label = {"Số hiệu chuyến bay "}
              onChangeText = {setFlyno}
          /> */}


          <View style = {styles.inputContainer}>
              <Text style=  {styles.title}>Số hiệu phương tiện</Text>
              <View style = {styles.borderPicker}>
                  <TextInput
                      onChangeText = {setFlyNo}
                      placeholder = {"Số hiệu phương tiện"}
                      style = {styles.textInput}
                      defaultValue = {flyNo}
                  />
              </View>
          </View>
          
          <View style = {{...styles.inputContainer}}>
              <Text style= {styles.label}>Thời gian khởi hành</Text>
              <View style = {styles.datetimeContainer}>

                  {showDepartureDate && 
                      <DateTimePicker
                          testID="dateTimePicker"
                          value={departureDate}
                          mode={'date'}
                          is24Hour={true}
                          onChange={onChangeDepartureDate}

                      />
                  }

                  <TouchableOpacity 
                      onPress = {()=>{
                          setShowDepartureDate(true);
                      }}
                  >
                          <View style = {styles.row}>
                              <Text  style= {styles.datetime}>
                                  {myDepartureDate[1]}
                              </Text>
                              <Icon name="calendar" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                          </View>
                  </TouchableOpacity>


                  {showDepartureTime && 
                      <DateTimePicker
                          testID="dateTimePicker1"
                          value={departureTime}
                          mode= "time"
                          is24Hour={true}
                          onChange={onChangeDepartureTime}
                      />
                  }
                  <TouchableOpacity 
                      onPress = {()=>{
                          setShowDepartureTime(true);
                      }}
                  >
                      <View style = {styles.row}>
                          <Text  style= {styles.datetime}>
                              {departureTime.toTimeString().substr(0,5)}
                          </Text>
                          <Icon name="time" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                      </View>
                  </TouchableOpacity>
                  
          
          </View>
          </View>


          <View style = {styles.inputContainer}>
              <Text style= {styles.label}>Thời gian đến</Text>

              <View style ={styles.datetimeContainer}>

                  {showDestinationDate && 
                          <DateTimePicker
                          
                              testID="dateTimePicker2"
                              value={landingDate}
                              mode={'date'}
                              is24Hour={true}
                              onChange={onChangeLandingDate}
                              minimumDate = {departureDate}
                          />
                      }

                      <TouchableOpacity 
                          onPress = {()=>{
                              setShowDestinationDate(true);
                          }}
                      >
                          <View style = {styles.row}>
                              <Text  style= {styles.datetime}>
                                  {myLandingDate[1]}
                              </Text>
                              <Icon name="calendar" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                          </View>
                      </TouchableOpacity>

                      {showDestinationTime && 
                          <DateTimePicker
                              testID="dateTimePicker3"
                              value={landingTime}
                              mode= "time"
                              is24Hour={true}
                              onChange={onChangeLandingTime}

                          />
                      }
                      <TouchableOpacity 
                          onPress = {()=>{
                              setShowDestinationTime(true);
                          }}
                      >
                          <View style = {styles.row}>
                              <Text  style= {styles.datetime}>
                                  {landingTime.toTimeString().substr(0,5)}
                              </Text>
                              <Icon name="time" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                          </View>
                      </TouchableOpacity>
              </View>
                  
                  
          </View>



      </View>
      </ScrollView>
              <View style = {styles.groupButtons}>
                <Button
                  onPress = {handleSubmit}
                  title = {"Chỉnh sửa"}
                  titleStyle = {styles.submitButton}
                />
                <Button
                  onPress = {()=>{
                      setIsShow(false);
                  }}
                  title = {"Quay lại"}
                  titleStyle = {styles.cancelButton}
                />
            </View>
            
        </View>
      </Modal>
    </View>
  );
}

