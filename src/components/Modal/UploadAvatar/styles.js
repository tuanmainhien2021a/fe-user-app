import {StyleSheet} from 'react-native'
import _styles from '../../../styles/style'
const size = 32;
const previewScale = 6;
const styles = StyleSheet.create({
    container : {
        // backgroundColor : '#e1e6ed',
        backgroundColor : 'white',
        // justifyContent : 'center',
        // alignItems : 'center',
        borderRadius : 20,
        borderWidth : 2,
    },

    header: {
        ..._styles.header
    },

    

    groupButton : {
        flexDirection : 'row',
        justifyContent : 'space-around',
        paddingVertical : 20,
    },
    
    submitButton  :{
        ..._styles.colorButton,
        paddingHorizontal : 10,
    },

    backButton  : {
        ..._styles.cancelButton,
        paddingHorizontal : 10,
    },

    icon : {
        width : size,
        height : size,
    },
    row_center : {
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'space-around'
    },

    title  :{
        fontSize : 20,
    },
    previewImage  :  {
        width : size * previewScale,
        height : size * previewScale,
        borderRadius : size * previewScale/2,
    },

    previewContainer : {
        justifyContent : 'center',
        alignItems : 'center'
    },

    // preview: {
    //     flex: 1,
    //     justifyContent: 'flex-end',
    //     alignItems: 'center',
    //   },
    //   capture: {
    //     flex: 0,
    //     backgroundColor: '#fff',
    //     borderRadius: 5,
    //     padding: 15,
    //     paddingHorizontal: 20,
    //     alignSelf: 'center',
    //     margin: 20,
    //   },
    cameraContainer : {
        // flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    }
    
});


export default styles;