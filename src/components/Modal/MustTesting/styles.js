import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container : {
        // backgroundColor : '#e1e6ed',
        backgroundColor : 'white',
        // justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 20,
        borderWidth : 2,
    },

    title : {
        fontSize : 25,
        textAlign : 'center',
    },

    input : {
        marginVertical : 20,
        
        borderWidth : 2,
        borderRadius : 5,
        display : 'flex',
        width : 200,
    },

    groupButton : {
        paddingVertical : 20,
        display : 'flex',
        flexDirection : 'row',
        width : 250,
        justifyContent :'space-between'
        // justifyContent :'flex-start'
    },
    
    button : {
        fontSize : 20,
        fontWeight : 'bold',
        paddingVertical : 10,
        minWidth : 115,
        textAlign  : 'center',
    },

    confirm :{
        backgroundColor : "#10e04e"
    },

    cancel : {
        borderWidth : 1,
        borderTopWidth : 1.5,
        borderStartWidth : 1.5,
        borderColor : "#10e04e",
    }
    
});


export default styles;