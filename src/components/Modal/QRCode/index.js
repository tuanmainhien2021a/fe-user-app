import React from 'react';
import { View,Text } from 'react-native';
import Modal from 'react-native-modal';
import QRCode from 'react-native-qrcode-svg';


import Button from '../../Button'
import styles from './styles';
export default function (props) {
  const {isShow,setIsShow,email} = props;
  const onCancel = () =>{
      setIsShow(false);
  }




  return (
    <View >
      <Modal isVisible={isShow}>
        <View style={styles.container}>
          <Text style ={styles.header}>Mã QR Code của bạn:</Text>
          <View style = {{padding : 50}}>
            <QRCode
              value = {JSON.stringify({email : email})}
              size = {200}
            />
          </View>
          <View style = {styles.buttonContainer}>
            <Button
              title = {"Đóng"}
              onPress = {onCancel}
              titleStyle = {styles.cancelButton}
            />
          </View>


        </View>
      </Modal>
    </View>
  );
}

