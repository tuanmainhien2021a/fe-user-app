import {StyleSheet} from 'react-native'
import _styles from '../../../styles/style'
const styles = StyleSheet.create({
    container : {
        // backgroundColor : '#e1e6ed',
        backgroundColor : 'white',
        // justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 20,
        borderWidth : 2,
    },

    header: {
        ..._styles.header,
    },

    cancelButton : {
        ..._styles.cancelButton,
        paddingHorizontal : 30,
    },

    buttonContainer : {
        alignItems : 'center',
        justifyContent : 'center',
        paddingVertical : 20
    }

    
});


export default styles;