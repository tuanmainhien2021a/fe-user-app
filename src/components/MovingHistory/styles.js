import {StyleSheet} from 'react-native'
import _styles from '../../styles/style'

const styles = StyleSheet.create({
    container : {
        justifyContent : 'center',
        alignItems : 'center',
        // backgroundColor : 'grey',
        flexDirection : 'row',
        borderWidth : 1,
        borderRadius : 20,
        marginVertical : 10,
        paddingHorizontal : 10,
        minHeight : 125,
        ..._styles.backgroundBorder,
    },

    col1 : {
        flex  : 5,
        paddingRight : 10,
        flexDirection : 'row'
    },

    address : {
        fontWeight : 'bold',
        fontSize : 18,
    },

    col2 : {
        flex : 3,
        alignItems : 'center'
    },

    thoigian : {
        flex : 6,
        justifyContent : 'center',

    },

    ngay : {
        flex : 4,
    },
    thoigianText : {
        fontSize : 25,
        fontWeight : 'bold',
        color : 'rgba(227, 29, 11,0.8)'
    },

    iconContainer : {
        alignItems  : 'center',

    },

    image : {
        width : 25,
        height : 25
    },

    title  :{
        fontSize : 18,
        fontWeight : 'bold'
    }


})


export default styles;