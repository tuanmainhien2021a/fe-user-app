import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';

import News from '../screens/News';
import NewsDetail from '../screens/News/NewsDetail';

export default function (props){
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator initialRouteName = "Homepage">
            <Stack.Screen
                name = {"Homepage"}
                component = {News}
                options = {{title : "Tin tức"}}

            />

            <Stack.Screen
                name = "Details"
                component = {NewsDetail}
                options = {{title : "Chi tiết"}}

            />
        </Stack.Navigator>
    )
}