import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import ForgotPassword from '../screens/Login/ForgotPassword';
import NewFormLogin from '../screens/Login/NewFormLogin'
import SignUpScreen from '../screens/Login/SignUpScreen';
import ChangePassword from '../screens/Login/ChangePassword';

export default function Login (props){
    const Stack = createStackNavigator();
    const {auth} = props;
    return (
    <Stack.Navigator initialRouteName = {NewFormLogin}>
        <Stack.Screen name="Login" component={NewFormLogin} auth = {auth}  options= {{headerShown : false}}/>
        <Stack.Screen name="SignUp" 
            component={SignUpScreen} options = {{title : "Đăng kí tài khoản"}}
            />
        <Stack.Screen name="ForgotPassword" component = {ForgotPassword} options = {{title : "Quên mật khẩu"}}/>
        <Stack.Screen name="ChangePassword" component = {ChangePassword} options = {{title : "Thay đổi mật khẩu"}}/>
        </Stack.Navigator>
    )
}