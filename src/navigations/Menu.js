import React, { Component } from 'react'
import {View} from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


import NewsNavigator from '../navigations/News'
import ProfileNavigator from '../navigations/Profile'
import CheckInLocation from '../screens/Check-in Location';
import DailyHealth from '../screens/Daily Heath'
const Tab = createBottomTabNavigator();
export default function Menu (){
    const Stack = createStackNavigator();
    return (
        <Tab.Navigator
          initialRouteName = {"Home"}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            switch (route.name) {
              case 'Home':
                iconName = focused
                ? 'home'
                : 'home-outline';
                break;
              case 'Check-in':
                iconName = focused
                ? 'location'
                : 'location-outline';
                break;
              case 'DailyHealth':
                iconName = focused
                ? 'medkit'
                : 'medkit-outline';
                break;
              case 'Me':
                iconName = focused
                ? 'person'
                : 'person-outline';
                break;
              
              default:
                break;
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          // activeTintColor: 'tomato',
          
          // inactiveTintColor: 'gray',
          activeBackgroundColor : 'rbga(50, 188, 207,0.4)',
          labelStyle : {fontSize : 15},
          // safeAreaInsets : {top : 25,},
          keyboardHidesTabBar : true,
          style  :{height : 60,borderTopWidth : 1.5,borderTopColor : 'black',backgroundColor : 'rgba(230, 223, 223,1)'}
          
        }}
      >
        <Tab.Screen name= "Home" component = {NewsNavigator} options = {{title : "Trang chủ"}}></Tab.Screen>
        <Tab.Screen name= "Check-in" component = {CheckInLocation} options = {{title : "Check in"}}></Tab.Screen>
        <Tab.Screen name= "DailyHealth" component = {DailyHealth} options = {{title : "X.Nghiệm"}}></Tab.Screen>
        <Tab.Screen name= "Me" component = {ProfileNavigator} options = {{title : "Cá nhân"}}></Tab.Screen>
        {/* <Tab.Screen name= "Me" component = {EditProfile}></Tab.Screen> */}
      </Tab.Navigator>
      
    )
}