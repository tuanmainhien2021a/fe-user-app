import DateTimePicker from '@react-native-community/datetimepicker';
import Geolocation from '@react-native-community/geolocation';
import React, { useEffect, useState } from 'react';
import { Alert, KeyboardAvoidingView, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Geocoder from 'react-native-geocoding';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Icon from 'react-native-vector-icons/Ionicons';

import userApi from '../../api/userApi';
import Button from '../../components/Button/';
import AsyncStorage from '../../storage';
import styles from './styles';
import Input from '../../components/Input'
import time2 from '../../utils/time2';

export default function(){
    Geocoder.init("AIzaSyBEj_N2fFz4FxUACprRCtZIBp21_r4LHu8"); // use a valid API key
    const [showDate, setShowDate] = useState(false);
    const [showTime, setShowTime] = useState(false);

    const [date,setDate] = useState(new Date(Date.now()));
    const [time,setTime] = useState(new Date(Date.now()));



    const [address,setAddress] = useState("");
    const [myLocation,setMyLocation] = useState({
        latitude : 12,
        longitude : 12,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,

    });

    useEffect(()=>{
        Geolocation.getCurrentPosition(info=>{
            setMyLocation({
                latitude: info.coords.latitude,
                longitude: info.coords.longitude,
                // latitude: info.coords.latitude,
                // latitude: info.coords.latitude,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
                
            });

            Geocoder.from(info.coords.latitude,info.coords.longitude)
                .then(res=>{
                    const address = res.results[0].formatted_address;
                    console.log("ABABAB",res);
                    console.log("999",address);
                    setAddress(address);
                })
                .catch(err=>{
                    console.log(123);
                    console.log(err);
                })

            
        })
        



        
    },[])

    const onChangeDate = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShowDate(false);
        setDate(currentDate);
      };

    const onChangeTime = (event, selectedTime) => {
        const currentTime = selectedTime || time;
        setShowTime(false);
        setTime(currentTime);
      };

    function formattedDate(date){
        let year = date.getFullYear();
        let month = date.getMonth()+1;
        if (month<10)
            month= `0${month}`;
        let day = date.getDate();
        if (day<10)
            day= `0${day}`;
        return `${year}-${month}-${day}`
    }

    function formattedTime(date){
        let hour = date.getHours();
        let minute = date.getMinutes();
        let second = date.getSeconds();
        if (hour<10)
            hour= `0${hour}`;
        if (minute<10)
            minue= `0${minute}`;
        if (second<10)
            second= `0${second}`;
        return `${hour}:${minute}:${second}`
    }

    const handleSubmit = () =>{
        AsyncStorage.read()
            .then(token=>{
                const data = {
                    address,
                    // time : new Date(formattedDate(date) + "T" + formattedTime(time))
                    time : time2.convertDateandTimeISOString(date,time)
                };
                return userApi.CheckInLocation(token,data)
                
            })
            .then(res=>{
                console.log(res);
                const {data} = res;
                if (res.isSuccess){
                    Alert.alert("Check-in thành công !!!");
                
                }
                else {
                    Alert.alert("Lỗi!!!");
                }
            })
            .catch(err=>{
                console.log(err);
            })
    }


    return (
        <ScrollView style = {styles.container}>
            <Text style = {styles.header}>Check-in địa điểm</Text>
            <View style = {styles.mapContainer}>
            <MapView
                provider={PROVIDER_GOOGLE} 
                style={styles.map}
                region={myLocation}
                
                >
                <Marker
                    draggable = {true}
                    coordinate = {myLocation}
                    onDrag = {(event)=>{

                        console.log(12312312);
                    }}

                    onDragEnd = {(event)=>{
                        setMyLocation({
                            ...myLocation,
                            latitude : event.nativeEvent.coordinate.latitude,
                            longitude : event.nativeEvent.coordinate.longitude,
                        })
                        console.log(event.nativeEvent.coordinate);
                        Geocoder.from(event.nativeEvent.coordinate)
                            .then(res=>{
                                const address = res.results[0].formatted_address;
                                // console.log("999",res);
                                setAddress(address);
                            })
                            .catch(err=>{
                                console.log(err);
                            })
                    }}
                    
                />
            </MapView>
            </View>
            <View style = {{flex : 1}}>

                    <Input
                        onChangeText = {setAddress}
                        value = {address}
                        multiline = {true}
                        label = {"Địa chỉ"}
                    />
  
                   <View style = {styles.inputContainer}>
                <Text style= {styles.title}>Thời gian đến</Text>

                <View style ={styles.datetimeContainer}>

                    {showDate && 
                            <DateTimePicker
                                testID="dateTimePicker2"
                                value={date}
                                mode={'date'}
                                is24Hour={true}
                                onChange={onChangeDate}
                                // minimumDate = {departureDate}
                            />
                        }

                        <TouchableOpacity 
                            onPress = {()=>{
                                setShowDate(true);
                            }}
                        >
                            <View style = {styles.row}>
                                <Text  style= {styles.datetime}>
                                    {formattedDate(date)}
                                </Text>
                                <Icon name="calendar" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                            </View>
                        </TouchableOpacity>

                        {showTime && 
                            <DateTimePicker
                                testID="dateTimePicker3"
                                value={time}
                                mode= "time"
                                is24Hour={true}
                                onChange={onChangeTime}
                            />
                        }
                        <TouchableOpacity 
                            onPress = {()=>{
                                setShowTime(true);
                            }}
                        >
                            <View style = {styles.row}>
                                <Text  style= {styles.datetime}>
                                    {time.toTimeString().substr(0,8)}
                                </Text>
                                <Icon name="time" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                            </View>
                        </TouchableOpacity>


                </View>
                    
                     <Button
                            title = "Check-in"
                            onPress = {handleSubmit}
                            titleStyle = {styles.submitButton}
                        />  
            </View>

                            
            </View>
        </ScrollView>
    )
}
