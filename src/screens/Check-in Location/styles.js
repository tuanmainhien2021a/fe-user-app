import { Dimensions, StyleSheet } from "react-native";
import _styles from '../../styles/style'

const styles = StyleSheet.create({


    map: {
        ...StyleSheet.absoluteFillObject,
      },

      container: {
        // alignItems: 'center',
        backgroundColor : 'white',
        flex : 1,
        paddingHorizontal : 10,

      },

      mapContainer : {
          width : Dimensions.get('screen').width-20,
          height : 300,
          paddingHorizontal : 10,

      },
      header : {
          textAlign : 'center',
          fontSize : 25,
          fontWeight  : 'bold',
          marginVertical : 20,
      },

    row : {
        flexDirection : 'row',
        alignItems  : 'center',
    },

    submitButton: {
        marginTop : 20,
        paddingHorizontal : 20,
        ..._styles.colorButton,
    },

    addressContainer  : {
        borderRadius : 10,
        borderWidth : 1,
        paddingVertical : 20,
        marginVertical  : 20,
    },

    inputContainer :{
        // borderWidth : 2,
        // borderRadius  : 15,
        // marginVertical : 20,
        // paddingVertical : 10,
        // paddingHorizontal : 30,
        paddingVertical : 10,
        paddingHorizontal : 15,
    },
    
    datetimeContainer : {
        borderWidth : 2,
        borderRadius : 20,
        // marginHorizontal : 10,
        flexDirection : 'row'
        ,paddingVertical : 10,
        alignItems : 'center',
        justifyContent : 'space-around'
    },
    

    title : {
        fontWeight : 'bold',
        fontSize : 15,
    },
})


export default styles;