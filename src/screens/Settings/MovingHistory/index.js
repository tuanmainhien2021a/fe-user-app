import React, { useCallback, useEffect, useState } from 'react';
import { FlatList, StyleSheet, Text, View,PermissionsAndroid, RefreshControl } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';


import userApi from '../../../api/userApi';
import MovingHistory from '../../../components/MovingHistory/index';
import AsyncStorage from '../../../storage';
import styles from './styles'

export default function (props) {
    const [mustUpdate,setMustUpdate] = useState(false);
    const [mustUpdate1,setMustUpdate1] = useState(0);
    const [data,setData] = useState([]);
    const [isLoading,setIsLoading] = useState (false);
    const [refreshing, setRefreshing] = useState(false);
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
      
    const onRefresh = useCallback(() => {
        setRefreshing(true);
        wait(500).then(() => setRefreshing(false));

        console.log(mustUpdate1);
        setMustUpdate1(mustUpdate1+1)
        console.log(mustUpdate1);
      }, [mustUpdate1]);
    

    useEffect(()=>{
        console.log("RENDER LAI NE");

        setIsLoading(true);
        AsyncStorage.read()
            .then(token=>{
                return userApi.MovingHistory(token)
            })
            .then(res=>{
                const {data} = res;
                setData(data);
                console.log("XX",data);
            })
            .catch(err=>{
                console.log(err);
            })
            .finally(()=>{
                setIsLoading(false);
            })
    },[mustUpdate])

    return (
        <View style = {styles.container}>
            <Spinner
                  visible={isLoading}
             />
            <Text style= {styles.header}> Lịch sử di chuyển</Text>
            <FlatList
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
                data = {data}
                renderItem = {({item})=>(
                    <MovingHistory
                        item = {item}
                        data = {data}
                        setData= {setData}
                        mustUpdate = {mustUpdate}
                        setMustUpdate = {setMustUpdate}
                    />
                )}
                keyExtractor = {item=>item.id+""}
            />
        </View>
    )
}

