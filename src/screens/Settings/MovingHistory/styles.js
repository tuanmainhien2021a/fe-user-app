import {StyleSheet} from 'react-native'

export default styles = StyleSheet.create({
    container :{
        flex : 1,
        backgroundColor : 'white',
        paddingHorizontal : 10,
    },

    header: {
        fontSize : 25,
        fontWeight : 'bold',
        textAlign : 'center',
        paddingVertical : 20,
    }
})