import React, { useEffect, useState } from 'react'
import { Alert, FlatList, StyleSheet, Text, TouchableOpacity, View,SafeAreaView,Image } from 'react-native';

import styles from './styles'
import Input from '../../../components/Input'
import Button from '../../../components/Button'
import AsyncStorage from '../../../storage'
import userApi from '../../../api/userApi';
import * as constants from '../../../constants'
import validation from '../../../utils/validation';
import validation1 from '../../../utils/validation1';

export default function (props) {
    const {navigation} = props;
    const [oldPassword,setOldPassword] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword,setConfirmPassword] = useState("");
    const [isPasswordErr,setIsPasswordErr] = useState(false);
    const [isConfirmPasswordErr,setIsConfirmPasswordErr] = useState(false);
    const [isOldPasswordErr,setIsOldPasswordErr] = useState(false);

    useEffect(()=>{
        setIsOldPasswordErr(!validation1.isPassword(oldPassword));
        setIsPasswordErr(!validation1.isPassword(password));
        setIsConfirmPasswordErr(!validation1.isComfirmPassword(password,confirmPassword))
      })
    function handleSubmit(){
        if ( isOldPasswordErr || isPasswordErr || isConfirmPasswordErr ){
            Alert.alert("Mật khẩu không hợp lệ!!!");
            return;
        }
        //PARAMS
        const params = {
            oldPassword,
            newPassword : password,
        };

        //call API
        AsyncStorage.read()
            .then(token=>{
                return userApi.ChangePassword(params,token)
            })
            .then(res=>{
                console.log(res);
                if(res.isSuccess){
                    Alert.alert("Đổi mật khẩu thành công");
                }
                else {
                    Alert.alert("Đổi mật khẩu thất bại!!!");
                }
            })
            .catch(err=>console.log(err))
    }

    return (
        <View style= {styles.container}>
            <Text style = {styles.header}> Thay đổi mật khẩu</Text>
            <Input  
                label ={"Mật khẩu cũ"} 
                placeholder= {"Nhập mật khẩu cũ"} 
                iconName = {"alert-circle-outline"} 
                onChangeText = {setOldPassword} 
                secureTextEntry= {true}  
                isErr= {isOldPasswordErr}
                errMsg={constants.PasswordErrMsg} 
            />
            <Input 
                label ={"Mật khẩu mới"} iconName = {"key-outline"} 
                placeholder= {"Nhập mật khẩu mới"} 
                secureTextEntry= {true}  onChangeText = {setPassword} 
                isErr= {isPasswordErr}
                errMsg={constants.PasswordErrMsg}     
            />
            <Input 
                label ={"Xác nhận mật khẩu"} placeholder= {"Nhập lại mật khẩu"} 
                iconName = {"sync-outline"} secureTextEntry= {true}  
                onChangeText = {setConfirmPassword} 
                isErr= {isConfirmPasswordErr}
                errMsg={constants.ConfirmPasswordErrMsg}
            />

            <View style = {{...styles.row,...styles.groupButtons}}>
                <Button
                    title = {"Cập nhật"}
                    onPress = {handleSubmit}
                    titleStyle = {styles.submitButton}

                />
                <Button
                    title = {"Quay lại"}
                    onPress = {()=>{navigation.goBack()}}
                    titleStyle = {styles.backButton}
                />
            </View>
        </View>
    )
}

