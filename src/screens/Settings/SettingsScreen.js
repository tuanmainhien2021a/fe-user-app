import React, { Component } from 'react'
import {View,Text,StyleSheet,FlatList, Alert} from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AuthContext from '../../context/AuthContext';

import ProfileButton from '../../components/ProfileButton'
import { useContext } from 'react';

export default function (props){
    const {signOut } = useContext (AuthContext);
    

    const {navigation} = props;
    console.log(navigation);
    const listButton = [{id:0,name:"Quản lý tài khoản",imageName : "manageProfile"},
                        {id:1,name:"Thay đổi mật khẩu",imageName : "changePassword"},
                        {id:2,name:"Lịch trình di chuyển",imageName : "movingHistory"}, 
                        // {id:3,name:"Quản lý xét nghiệm"},
                        {id:4,name:"Khai báo y tế",imageName : "healthDeclare"},
                        {id:5,name:"Đăng xuất",imageName : "logout"},

                    ];
    
    function Id0Click(){
        navigation.navigate("ManageProfile");
    }
    function Id1Click(){
        navigation.navigate("ChangePassword");
    }
    function Id2Click(){
        navigation.navigate("MovingHistory");
    }
    function Id3Click(){
        navigation.navigate("TestingManagement");
    }
    function Id4Click(){
        navigation.navigate("HealthDeclare");
    }
    function Id5Click(){
        signOut();
    }

    function handleClick (id){
        console.log("ID la:",id);
        switch (id) {
            case 0:
                Id0Click();
                break;
            case 1:
                Id1Click();
                break;
            case 2:
                Id2Click();
                break;
            case 3:
                Id3Click();
                break;
            case 4:
                Id4Click();
                break;
            case 5: 
                Id5Click();
                break;
            default:
                break;
        }
    }

        return (
            <View style = {styles.container}>
                <FlatList style ={styles.list}
                    data = {listButton}
                    renderItem ={({item}) => {
                        return (
                            <ProfileButton item = {item} onClick = {()=>
                                {
                                handleClick(item.id)
                            }}>

                            </ProfileButton>
                        )
                    }
                    }
                    keyExtractor = {item=>item.id}
                >

                </FlatList>
            </View>
        )
}
const styles = StyleSheet.create({
    container:{
        // textAlignVertical: "center",
        backgroundColor : 'white',
        // alignItems : 'center',
        justifyContent : 'center',
        flex  : 1,
        paddingTop : 50,

    },

    list : {
        // backgroundColor : 'pink'
    },
    button : {
        display : 'flex',
        paddingVertical : 15,
        borderWidth : 1,
    }
})

