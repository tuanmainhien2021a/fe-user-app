import {StyleSheet} from 'react-native'
import _styles from '../../../styles/style'
const size = 64;
const styles = StyleSheet.create({
    container :{
        ..._styles.container,
        paddingBottom : 10,
        // backgroundColor : 'red'
    },

    header :{
        ..._styles.header
    },
    row : {
        flexDirection : 'row',
        alignItems : 'center',
        paddingVertical : 5,
        paddingHorizontal : 15,
    },
    groupButtons : {
        marginTop   : 20,
        marginBottom : 50,
        justifyContent : 'space-around',

    },

    col1 : {
        flex : 3,
    },

    col2 : {
        flex : 7,
    },

    title : {
        fontWeight : 'bold',

    },

    submitButton : {
        ..._styles.colorButton,
        paddingHorizontal : 20,
        paddingVertical : 5,
    },

    backButton : {
        ..._styles.cancelButton,
        paddingHorizontal : 20,
        paddingVertical : 5,
        

    },
    avatarContainer : {
        alignItems : 'center',
        justifyContent : 'center',
        flexDirection : 'row',
        // backgroundColor : 'rgba(199, 194, 185,0.1)'
    },

    avatar : {
        width : size,
        height : size,
        borderRadius : 30,
    },

    editIcon : {
        width : size/2,
        height : size/2,
        marginLeft : 20,
    },



})

export default styles;