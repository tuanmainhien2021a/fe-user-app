import DateTimePicker from '@react-native-community/datetimepicker';
import { Picker } from '@react-native-picker/picker';
import countryList from 'country-list';
import React, { useEffect, useState } from 'react';
import { Alert, Image, Text, View } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';
import DocumentPicker from 'react-native-document-picker';


import userApi from '../../../api/userApi';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import AsyncStorage from '../../../storage';
import TimeFormatter from '../../../utils/time';
import styles from './styles';
import UploadAvatar from '../../../components/Modal/UploadAvatar';
import QRCode from '../../../components/Modal/QRCode';



export default function (props) {
    const {navigation} = props;
    const listGender = ["Nam","Nữ","Khác"];
    const [isShow,setIsShow] = useState(false);
    const [isShowQRCode,setIsShowQRCode] = useState(false);
    const [mustUpdate,setMustUpdate] = useState(false);
    const [avatarUrl,setAvatarUrl] = useState(null);
    const listCountry = countryList.getData().map(item=>item.name);
    //list State
    const [email,setEmail] = useState("");
    const [firstName,setFirstName] = useState("");
    const [lastName,setLastName] = useState("");
    const [gender,setGender] = useState("");
    const [nationality,setNationality] = useState("");
    const [phoneNumber,setPhoneNumber] = useState("");
    const [dateOfBirth,setDateOfBirth] = useState(new Date());
    const [address,setAddress] = useState("");
    const [data,setData] = useState({});
    const [loading,setLoading] = useState(true);
    const [singleFile, setSingleFile] = useState(null);

    let time = new TimeFormatter();

    // console.log(listCountry);
    //Date
    const [showDate,setShowDate] = useState(false);

    //call API
    useEffect(()=>{
        AsyncStorage.read()
        .then(token =>{
            return userApi.getProfile(token);
        })
        //get response
        .then(res=>{
            const {data} = res
            console.log("GET",data);
            setData(data);
            setDateOfBirth(new Date(data.dateOfBirth));
            setNationality(data.nationality);
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setPhoneNumber(data.phoneNumber);
            setAddress(data.address);
            setGender(data.gender);
            setEmail(data.email);

        })

        .catch(err=>console.log(err))
        .finally(()=>{
            setLoading(false);
        })

    },[])

    useEffect(()=>{
        AsyncStorage.read()
            .then(token=>userApi.GetAvatar(token))
            .then(res=>{
                const {data} = res;
                setAvatarUrl(data+"?a="+Math.random());
                console.log("LInk",data);
            })
            .catch(err=>console.log(err))
    },[mustUpdate])

    const onChangeDate = (event, selectedDate) => {
        const currentDate = selectedDate || dateOfBirth;
        setShowDate(false);
        setDateOfBirth(currentDate);
      };



    function handleSubmit(){
        const params = {
            accountId : data.accountId,
            firstName,
            lastName,
            phoneNumber,
            dateOfBirth : dateOfBirth.toISOString().substr(0,10),
            gender,
            nationality,
            address,
        };

        console.log("PARAM",params);
        AsyncStorage.read()
            .then(token=>{
                return userApi.UpdateProfile(params,token)
            })
            .then(res=>{
                if(res.isSuccess){
                    Alert.alert("Cập nhật thông tin thành công");
                }
                else {
                    Alert.alert("Lỗi !!!")
                }
                const {data} = res;
                setData(data);
                
            })
            .catch(err=>console.log(err))
    }
    console.log("RENDER LAI");
    return (
        <ScrollView style = {styles.container}>
            <UploadAvatar
                isShow = {isShow}
                setIsShow = {setIsShow}
                mustUpdate = {mustUpdate}
                setMustUpdate = {setMustUpdate}
                avatarURL  = {avatarUrl}
            />
            <QRCode
                isShow = {isShowQRCode}
                setIsShow = {setIsShowQRCode}
                email = {email}
            />
            <Spinner
                visible={loading}
            />
            <Text style = {styles.header}>
                Thông tin cá nhân
            </Text>
            <View style = {{justifyContent : 'space-around',alignItems : 'center',flexDirection : 'row'}}>
            <TouchableOpacity
                    onPress = {()=>{
                        setIsShow(true)
                    }}
                >
            <View style = {styles.avatarContainer}>
                {avatarUrl==null? <Image
                    source = {require('../../../assets/manageProfile.png')}
                    style = {styles.avatar}
                />: 
                <Image
                    source = {{
                        uri : avatarUrl
                    }}
                    style = {styles.avatar}
                />}


                <Image
                    source = {require('../../../assets/more.png')}
                    style = {styles.editIcon}
                />
            </View>
            </TouchableOpacity>
            <TouchableOpacity
                onPress = {()=>{
                    setIsShowQRCode(true);
                }}
            >
                <View style ={styles.avatarContainer}>
                    <Text style ={{fontSize : 20,fontWeight :'bold'}}>QRCode</Text>
                    <Image
                        source = {require('../../../assets/qrcode.png')}
                        style = {styles.editIcon}
                    />
                </View>
            </TouchableOpacity>
            </View>
            <Input
                label = "Họ"
                onChangeText = {setLastName}
                iconName = {"person-outline"}
                value = {lastName}
            />

            <Input
                label = "Tên"
                onChangeText = {setFirstName}
                iconName = {"person-outline"}
                value = {firstName}
            />

            <Input
                label = "Số điện thoại"
                onChangeText = {setPhoneNumber}
                iconName = {"call-outline"}
                value = {phoneNumber}
            />

            <Input
                label = "Địa chỉ"
                onChangeText = {setAddress}
                iconName = {"location-outline"}
                value = {address}
                multiline = {true}
            />

            <View style ={styles.row}>
                <View style ={styles.col1}>
                    <Text style = {styles.title}>Ngày sinh:</Text>
                </View>

                <View style = {styles.col2}>
                    {showDate && 
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={dateOfBirth}
                        mode={'date'}
                        is24Hour={true}
                        onChange={onChangeDate}
                    />

                    }
                    <TouchableOpacity 
                        onPress = {()=>{
                            setShowDate(true);
                        }}
                    >
                        <View style = {styles.row}>
                            <Text  style= {styles.datetime}>
                                {dateOfBirth.toISOString().substr(0,10)}
                            </Text>
                            <Icon name="calendar" size={30} color="#34c2ed" style = {{paddingLeft : 20}} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>

            {/* gender */}
            <View style={styles.row}>
                <View style = {styles.col1}>
                  <Text style = {styles.title}>Giới tính: </Text>
                </View>
                
                <View style = {styles.col2}>
                    <Picker
                        selectedValue={gender}
                        mode = {'dropdown'}
                        onValueChange={ itemValue =>
                            setGender(itemValue)
                        }
                        
                        >
                        {listGender.map(value=>(
                            <Picker.Item  label ={value} value = {value} key={value}></Picker.Item>
                        ))}
                    </Picker>
                </View>
            </View>

            <View style = {styles.row}>
                <View style= {styles.col1}>
                    <Text style = {styles.title}>Quốc gia:</Text>
                </View>

                <View style ={styles.col2}>
                    <Picker
                        selectedValue={nationality}
                        mode = {'dialog'}
                        onValueChange={ itemValue =>{
                            console.log(itemValue);
                        return setNationality(itemValue);
                        }
                        }

                        
                    >
                        {listCountry.map(value=>(
                            <Picker.Item  label ={value} value = {value} key={value}></Picker.Item>
                        ))}
                    </Picker>
                </View>
            </View>



            {/* 3 buttons */}
            <View style = {{...styles.row,...styles.groupButtons}}>
                <Button
                    title = {"Cập nhật"}
                    onPress = {handleSubmit}
                    titleStyle = {styles.submitButton}

                />
                <Button
                    title = {"Quay lại"}
                    onPress = {()=>{navigation.goBack()}}
                    titleStyle = {styles.backButton}
                />
            </View>



        </ScrollView>
    )
}

