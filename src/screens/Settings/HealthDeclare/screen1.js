import DateTimePicker from '@react-native-community/datetimepicker';
import { Picker } from '@react-native-picker/picker';
import React, { useEffect, useState } from 'react';
import { Alert, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import Toast from 'react-native-simple-toast';

import cityApi from '../../../api/cityApi';
import userApi from '../../../api/userApi';
import Button from '../../../components/Button';
import MustTestingModal from '../../../components/Modal/MustTesting';
import Input from '../../../components/Input'
import AsyncStorage from "../../../storage";
import styles from './styles';
import time2 from '../../../utils/time2';



export default function (props){
    //init state

    const {navigation} = props;
    const {tabBarNavigation} = props;
    // console.log("TABBAR",tabBarNavigation);
    const [departureCityId,setdepartureCityId] = useState(0);
    const [destinationCityId,setDestinationCityId] = useState(9);

    const [listCity,setListCity] = useState([]);
    const [flyNo,setFlyno] = useState("");

    const [departureDate,setDepartureDate] = useState(new Date(Date.now()));
    const [departureTime,setDepartureTime] = useState(new Date(Date.now()));

    const [landingDate,setLandingDate] = useState(new Date(Date.now()));
    const [landingTime,setLandingTime] = useState(new Date(Date.now()));

    const [showDepartureDate,setShowDepartureDate] = useState(false);
    const [showDepartureTime,setShowDepartureTime] = useState(false);

    const [showDestinationDate,setShowDestinationDate] = useState(false);
    const [showDestinationTime,setShowDestinationTime] = useState(false);

    //modal
    const [isShow,setIsShow] = useState(false);

    //component did mount

    useEffect(()=>{
        AsyncStorage.read()
            .then((token)=>{
                return cityApi.getCities(token); 
            })
            .then(res=>{
                const {data} = res;
                setListCity(data);
                // console.log("DANH SACH THANH PHO :",data);
            })
            .catch(err=>{
                console.log(err);
            })
    },[])


    function formattedDate(date){
        let year = date.getFullYear();
        let month = date.getMonth()+1;
        if (month<10)
            month= `0${month}`;
        let day = date.getDate();
        if (day<10)
            day= `0${day}`;
        return `${year}-${month}-${day}`
    }

    function formattedTime(date){
        let hour = date.getHours();
        let minute = date.getMinutes();
        let second = date.getSeconds();
        if (hour<10)
            hour= `0${hour}`;
        if (minute<10)
            minue= `0${minute}`;
        if (second<10)
            second= `0${second}`;
        return `${hour}:${minute}:${second}`
    }

    const onChangeDepartureDate = (event, selectedDate) => {
        const currentDate = selectedDate || departureDate;
        setShowDepartureDate(false);
        setDepartureDate(currentDate);
      };

    const onChangeDepartureTime = (event, selectedDate) => {
        const currentDate = selectedDate || departureTime;
        setShowDepartureTime(false);
        setDepartureTime(currentDate);
      };

    const onChangeLandingDate = (event, selectedDate) => {
        const currentDate = selectedDate || landingDate;
        setShowDestinationDate(false);
        setLandingDate(currentDate);
      };
    const onChangeLandingTime = (event, selectedDate) => {
        const currentDate = selectedDate || landingTime;
        setShowDestinationTime(false);
        setLandingTime(currentDate);
      };

    const handleSubmit = ()=>{
        AsyncStorage.read()
            .then(token=>{
                const data = {
                    departureCityId,
                    destinationCityId,
                    flyNo,
                    // departureTime : new Date(formattedDate(departureDate)+"T"+formattedTime(departureTime)),
                    departureTime : time2.convertDateandTimeISOString(departureDate,departureTime),
                    landingTime : time2.convertDateandTimeISOString(landingDate,landingTime),
                }
                if (data.departureTime>=data.landingTime){
                    Toast.show("Thời gian không phù hợp, vui lòng thử lại",Toast.LONG);
                    return;
                }
                console.log("data",data);
                return userApi.UpdateItinerary(token,data);
            })
            .then(res=>{
                const {data} = res;
                console.log("DT",res);
                if (res.isSuccess) {
                    // Alert.alert("Thêm lịch trình thành công");
                    //must tesing condition
                    if(data.mustTesting){
                        setIsShow(true);
                    }
                    else {
                    Toast.show("Khai báo y tế thành công",Toast.SHORT);
                    }
                }
                else {
                    Alert.alert("Lỗi!!!");
                }
            })
            .catch(err=>{
                const {data} = err;
                console.log("Lỗi:",data);
                if(!data.isSuccess){
                    Toast.show("Thông tin lịch trình không hợp lệ",Toast.LONG);
                }
            })
    }

    return(
        <ScrollView>
      
        <View style= {styles.container}>
            <MustTestingModal
                isShow = {isShow}
                setIsShow = {setIsShow}
                navigation = {tabBarNavigation}
            />
            <Text style= {styles.header}> Lịch trình di chuyển </Text>

            <View style = {styles.inputContainer}>
                <Text style= {styles.label}>Nơi khởi hành</Text>
                <View style = {styles.borderPicker}>
                    <Picker
                            selectedValue={departureCityId}
                            mode = {'dialog'}
                            onValueChange={(itemValue, itemIndex) =>
                                setdepartureCityId(itemValue)
                            }>
                            {listCity.map((value,index)=>{
                                const enabled = value.id != destinationCityId;
                                // console.log(departureCityId,index,enabled);

                                return (
                                    <Picker.Item 
                                        // style = {enabled? {} : {backgroundColor : 'red',color : 'red'}} 
                                        label ={value.name} 
                                        value = {value.id} 
                                        key={value.id}
                                        enabled = {enabled}
                                    />
                                    )
                            })}
                    </Picker>
                </View>
            </View>

            <View style = {styles.inputContainer}>
                <Text style= {styles.label}>Nơi đến</Text>
                <View style ={styles.borderPicker}>
                    <Picker
                            selectedValue={destinationCityId}
                            mode = {'dialog'}
                            onValueChange={ itemValue =>
                                setDestinationCityId(itemValue)
                            }
                            
                            >
                            {
                            listCity.map((value,index)=>{
                                const enabled = departureCityId != value.id
                                console.log(departureCityId,index,enabled);
                                return (
                                    <Picker.Item 
                                        // style = {enabled? {color : 'red'} : styles.disableDate } 
                                        label ={value.name} 
                                        value = {value.id} 
                                        key={value.id}
                                        enabled = {enabled}
                                        
                                    />)
                            })}
                    </Picker>
                </View>
            </View>
{/* 
            <Input
                label = {"Số hiệu chuyến bay "}
                onChangeText = {setFlyno}
            /> */}


            <View style = {styles.inputContainer}>
                <Text style=  {styles.title}>Số hiệu phương tiện</Text>
                <View style = {styles.borderPicker}>
                    <TextInput
                        onChangeText = {setFlyno}
                        placeholder = {"Số hiệu phương tiện"}
                        style = {styles.textInput}
                    />
                </View>
            </View>
            
            <View style = {{...styles.inputContainer}}>
                <Text style= {styles.label}>Thời gian khởi hành</Text>
                <View style = {styles.datetimeContainer}>

                    {showDepartureDate && 
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={departureDate}
                            mode={'date'}
                            is24Hour={true}
                            onChange={onChangeDepartureDate}
                        />
                    }

                    <TouchableOpacity 
                        onPress = {()=>{
                            setShowDepartureDate(true);
                        }}
                    >
                            <View style = {styles.row}>
                                <Text  style= {styles.datetime}>
                                    {formattedDate(departureDate)}
                                </Text>
                                <Icon name="calendar" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                            </View>
                    </TouchableOpacity>


                    {showDepartureTime && 
                        <DateTimePicker
                            testID="dateTimePicker1"
                            value={departureTime}
                            mode= "time"
                            is24Hour={true}
                            onChange={onChangeDepartureTime}
                        />
                    }
                    <TouchableOpacity 
                        onPress = {()=>{
                            setShowDepartureTime(true);
                        }}
                    >
                        <View style = {styles.row}>
                            <Text  style= {styles.datetime}>
                                {departureTime.toTimeString().substr(0,5)}
                            </Text>
                            <Icon name="time" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                        </View>
                    </TouchableOpacity>
                    
            
            </View>
            </View>


            <View style = {styles.inputContainer}>
                <Text style= {styles.label}>Thời gian đến</Text>

                <View style ={styles.datetimeContainer}>

                    {showDestinationDate && 
                            <DateTimePicker
                                testID="dateTimePicker2"
                                value={landingDate}
                                mode={'date'}
                                is24Hour={true}
                                onChange={onChangeLandingDate}
                                minimumDate = {departureDate}
                            />
                        }

                        <TouchableOpacity 
                            onPress = {()=>{
                                setShowDestinationDate(true);
                            }}
                        >
                            <View style = {styles.row}>
                                <Text  style= {styles.datetime}>
                                    {formattedDate(landingDate)}
                                </Text>
                                <Icon name="calendar" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                            </View>
                        </TouchableOpacity>

                        {showDestinationTime && 
                            <DateTimePicker
                                testID="dateTimePicker3"
                                value={landingTime}
                                mode= "time"
                                is24Hour={true}
                                onChange={onChangeLandingTime}
                            />
                        }
                        <TouchableOpacity 
                            onPress = {()=>{
                                setShowDestinationTime(true);
                            }}
                        >
                            <View style = {styles.row}>
                                <Text  style= {styles.datetime}>
                                    {landingTime.toTimeString().substr(0,5)}
                                </Text>
                                <Icon name="time" size={30} color="#34c2ed" style = {{paddingLeft : 10}} />
                            </View>
                        </TouchableOpacity>
                </View>
                    
                    
            </View>

            <Button
                title = {"Đăng ký"}
                onPress = {handleSubmit}
                titleStyle = {styles.titleStyle}
            />

        </View>
        </ScrollView>
    )
}