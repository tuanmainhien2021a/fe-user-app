import React, { useContext, useState,useRef,useEffect} from 'react';
import { Alert, StyleSheet, Text, View ,async} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import Button from '../../components/Button';
import Input from '../../components/Input';
import Logo from '../../components/Logo';
import AuthContext from '../../context/AuthContext';
import loginApi from '../../api/loginApi'
import _styles from '../../styles/style'
import ConfirmPassModal from '../../components/Modal/ConfirmPass'
import AsyncStorage from '../../storage'
import validation from '../../utils/validation';
import * as constants from '../../constants'
import validation1 from '../../utils/validation1';

export default function NewFormLogin(props) {
    const [email,setEmail] = useState("buivanhoa99@gmail.com");
    const [password, setPassword] = useState('Bui12345');
    const [visibleModal,setVisibleModal] = useState(false);
    const [error,setError] = useState(false);
    const [loading,setLoading] = useState(false);
    const [isPassErr,setIsPassErr] = useState(false);
    const [isEmailErr,setIsEmailErr] = useState(false);
    // console.log("EMAIL : ",email);
    // console.log("PASSWORD: ",password);
    const refModal = useRef(null);
    const {signIn } = useContext(AuthContext);
    const {navigation} = props;

    useEffect (()=>{
      setIsPassErr(!validation1.isPassword(password))
      setIsEmailErr(!validation1.isEmail(email))
    })
    const LoginOnPress = ()=>{
      if (isEmailErr || isPassErr){
        Alert.alert("Tài khoản hoặc mật khẩu không hợp lệ!")
        return;
      }
      setLoading(true)
      loginApi.login({
        email : email,
        password : password,
      })
      .then(res=>{
        const {data} = res;
        console.log("RES ne999999999999999 ", res);
        //Error 400
        // if (typeof data == undefined) return;
        console.log("LOGIN ne");
        //wrong user || pass
        if (!res.isSuccess || typeof res === undefined){
            setError(true);
            setTimeout(()=>{
              setError(false);
            },3000)
            console.log("sai");
        }
        // Unverified
        if (!data.isVerified){
          refModal.current.toggleModal();
          // console.log("Unverified");
          setLoading(false);
        }
        // Successful
        else {
          // setLoading(false)
          signIn(email,password);
          AsyncStorage.write("userToken",data.token);
          AsyncStorage.write('id',data.id+"");
          console.log("ID NE:",data.id);
        }   
      })
      .catch(err=>{
        console.log(err);
        setLoading(false)

      })
      .finally(()=>{
        // setLoading(false)
      })
      return ()=>{
        setLoading(false);
      }
    }

    const SignUpOnPress = ()=>{
      navigation.navigate("SignUp");
    }
    const ForgotPasswordOnPress = ()=>{
      navigation.navigate("ForgotPassword");
    }
    const  onPress = () =>{
    }
    const toggleModal = ()=>{
      setVisibleModal(!visibleModal);
      console.log(visibleModal);
    }
    
    return (
      <View style = {{..._styles.container,...styles.container}}>
        <Spinner
          visible={loading}
        />
        <ConfirmPassModal ref={refModal} email = {email} password = {password} />
          <Logo></Logo>
          <View style = {styles.loginSection}>
            <Input 
              label ={"Email"} 
              iconName = {"mail-outline"}   
              onChangeText = {setEmail} 
              keyboardType= {"email-address"} 
              isErr = {isEmailErr}              
              errMsg={constants.EmailErrMsg} 
            />
            <Input 
              label ={"Mật khẩu"} 
              iconName = {"key-outline"}  
              onChangeText = {setPassword} 
              secureTextEntry = {true} 
              isErr = {isPassErr}
              errMsg={constants.PasswordErrMsg}
            />

            {error && <Text style={styles.error}>Email hoặc mật khẩu không đúng</Text>}

            <Button 
              onPress = {LoginOnPress} 
              title = {"Đăng nhập"} 
              titleStyle = {styles.loginButton} 
              />
          </View>
          <View style = {styles.ForgotPass}>
             <Text >Quên mật khẩu?</Text>
             <Button 
              title = {"Tìm mật khẩu"} 
              titleStyle={styles.title} 
              onPress = {ForgotPasswordOnPress}
              />
          </View>

          <View style = {styles.SignUp}>
             <Text  >Chưa có tài khoản?</Text>
             <Button 
              title = {"Đăng kí"} 
              titleStyle={styles.title}  
              onPress = {SignUpOnPress}
              />
          </View>


        </View>
  )
  }


  const styles = StyleSheet.create({
    container : {
    },
    loginButton : {
        ..._styles.colorButton,
        marginTop : 20,
        width : 300,
    },
    SignUp : {
        textAlign : 'center',
        justifyContent : 'center',
        alignItems : 'flex-end',
        flexDirection : 'row',
        paddingVertical : 10,
    },

    ForgotPass : {
            borderTopWidth : 1,
            borderBottomWidth : 1,
            textAlign : 'center',
            marginTop : 20,
            justifyContent : 'center',
            alignItems : 'flex-start',
            display : 'flex',
            flex : 1,
            flexDirection : 'row',
            padding : 10,
            backgroundColor : 'white'
            
    },
    loginSection : {
      // backgroundColor : '#dfebe1',
      backgroundColor : 'white',
      borderRadius : 20,
      paddingBottom : 20,
      borderWidth : 1,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 12,
      },
      shadowOpacity: 0.58,
      shadowRadius: 16.00,

      elevation: 24,

    } 
    ,

    input: {
        flex: 1,
        color: '#424242',
        fontWeight : 'bold',
    },

    title : {
        fontWeight : 'bold',
        textAlign : 'center',
        borderRadius : 20,
        textDecorationLine : 'underline',
        marginLeft : 10,
        fontSize : 15,
    },

    error : {
      fontSize : 20,
      paddingHorizontal : 20,
      color : 'red',

    }

})

