import React, { useContext, useEffect, useState } from 'react';
import { Alert, StyleSheet, Text, View ,KeyboardAvoidingView,ScrollView} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';


import Button from '../../components/Button';
import Input from '../../components/Input/';
import Logo from '../../components/Logo';
import AuthContext from '../../context/AuthContext';
import _styles from '../../../src/styles/style'
import loginApi from '../../api/loginApi'
import * as constants from '../../constants'
import validation from '../../utils/validation';
import validation1 from '../../utils/validation1';

export default function SignUpScreen(props) {
    const [username, setUsername] = useState('');
    const [firstName,setFirstName] = useState('');
    const [lastName,setLastName] = useState('');
    const [email,setEmail] = useState("");
    const [pass,setPass] = useState("");
    const [conFirmPass,setConFirmPass] = useState("");
    const [loading,setLoading] = useState(false);

    const [isPasswordErr,setIsPasswordErr] = useState(false);
    const [isConfirmPasswordErr,setIsConfirmPasswordErr] = useState(false);
    const [isEmailErr,setIsEmailErr] = useState(false);


    useEffect(()=>{
      setIsEmailErr(!validation1.isEmail(email))
      setIsPasswordErr(!validation1.isPassword(pass));
      setIsConfirmPasswordErr(!validation1.isComfirmPassword(pass,conFirmPass))
    })
    
    const {signIn } = useContext(AuthContext);

    const {navigation} = props;
    const LoginOnPress = ()=>{
      signIn("1","1");
    }

    const SignUpOnPress = ()=>{
      navigation.navigate("SignUp");
    }
    const ForgotPasswordOnPress = ()=>{
      navigation.navigate("ForgotPassword");
    }
    const onPress = () =>{
        if (isPasswordErr || isConfirmPasswordErr || isEmailErr ){
          Alert.alert("Tài khoản hoặc mật khẩu không hợp lệ");
          return;
        }
        setLoading(true);
        const response = loginApi.register(
          {
            email:email,
            password:pass,
            firstName : firstName,
            lastName :  lastName,
          })
          .then(res=>{
            // console.log(res);
            if (res.isSuccess){
              Alert.alert("Đăng kí thành công");
              navigation.goBack();
            }
            else {
              Alert.alert("Đăng kí không thành công");
            }
          })
          .catch(err=>{
            console.log(err);
          })
          .finally(()=>{
            setLoading(false);
          })
        
    }

    return (
      <ScrollView>
      <View style = {{..._styles.container,...styles.container}}>
          <Spinner
            visible={loading}
          />
          <Logo></Logo>
          {/* <KeyboardAvoidingView 
                 behavior = 'position'
          > */}
            <Input label ={"Email"} iconName = {"mail-outline"}  onChangeText = {setEmail} keyboardType= {"email-address"}  
              isErr = {isEmailErr}              
              errMsg={constants.EmailErrMsg} 
            ></Input>
            
            <Input label ={"Họ"} iconName = {"person-outline"}  onChangeText = {setFirstName}    ></Input>
            <Input label ={"Tên"} iconName = {"person-outline"}  onChangeText = {setLastName}   ></Input>
            <Input label ={"Mật khẩu"} iconName = {"key-outline"} secureTextEntry= {true}  onChangeText = {setPass} 
              errMsg={constants.PasswordErrMsg} 
              isErr = {isPasswordErr}
            ></Input>
            <Input label ={"Xác nhận mật khẩu"} placeholder= {"Nhập lại Mật khẩu"} iconName = {"key-outline"} secureTextEntry= {true}  onChangeText = {setConFirmPass} 
              errMsg={constants.ConfirmPasswordErrMsg} 
              isErr = {isConfirmPasswordErr}

            ></Input>
         {/* </KeyboardAvoidingView> */}
            
            <Button onPress={onPress} title = {"Đăng kí"} titleStyle = {styles.button}></Button>

        </View>
        </ScrollView>
  )
  }


  const styles = StyleSheet.create({
    container : {
        flex : 1,
    },
    button : {
      width : 250,
      marginVertical : 30,
      ..._styles.colorButton,
    },
   
})

