import React, { useState } from 'react';
import { useEffect } from 'react';
import {View,Text,StyleSheet, TextInput, Alert,ScrollView, PointPropType} from 'react-native';
import {FlatList} from 'react-native-gesture-handler'
import {Picker} from '@react-native-picker/picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import Toast from 'react-native-simple-toast';



import testingApi from '../../api/testingApi';
import userApi from '../../api/userApi';
import Switch from '../../components/Switch'
import AsyncStorage from '../../storage';
import {  TouchableOpacity } from 'react-native-gesture-handler';
import Button from '../../components/Button'
import _styles from '../../styles/style'
import cityApi from '../../api/cityApi';
import time2 from '../../utils/time2';
export default function (props) {
    const [data,setData] = useState ([]);

    const [date, setDate] = useState(new Date(Date.now()));
    const [time, setTime] = useState(new Date(Date.now()));



    const [mode, setMode] = useState('datetime');
    const [show, setShow] = useState(false);
    const [showTime, setShowTime] = useState(false);
    const [cities,setCities] = useState([]);
    const [idCity,setIdCity] = useState();
    const [idLocation, setIdLocation] = useState();
    const [filteredLocation,setFilteredLocation] = useState([]);

    //time for choosing
    let listTime = [
        "8","9","10","14","15","16"
    ];
    if (date.getDay()== new Date(Date.now()).getDay()){
        listTime = listTime.filter(time => time > new Date(Date.now()).getHours());
    }
    
    const [timeHour, setTimeHour] = useState(listTime[0]);

    // state for symptoms
    const [asthma,setAsthma] = useState(false);
    const [pregnancy,setPregnancy] = useState(false);
    const [highBloodPressure,setHighBloodPressure] = useState(false);
    const [obesity,setObesity] = useState(false);
    const [heartProblem,setHeartProblem] = useState(false);
    const [hiv,setHiv] = useState(false);
    const [cough,setCough] = useState(false);
    const [fever,setFever] = useState(false);
    const [shortnessOfBreath,setShortnessOfBreath] = useState(false);
    const [runningNose,setRunningNose] = useState(false);
    const [tiredness,setTiredness] = useState(false);
    const [specialSymptom,setSpecialSymptom] = useState("");

    const listSymptoms = [
        {status: asthma,setStatus: setAsthma ,eng:"asthma",vie :"Hen suyễn"},
        {status: pregnancy, setStatus: setPregnancy,eng:"pregnancy",vie :"Mang thai"},
        {status: highBloodPressure, setStatus: setHighBloodPressure,eng:"highBloodPressure",vie :"Huyết áp"},
        // {status: obesity, setStatus: setObesity,eng:"obesity",vie :"Béo phì"},
        {status: heartProblem, setStatus: setHeartProblem,eng:"heartProblem",vie :"Bệnh tim"},
        {status: hiv, setStatus: setHiv,eng:"hiv",vie :"HIV"},
        {status: cough, setStatus: setCough,eng:"cough",vie :"Ho"},
        {status: fever, setStatus: setFever,eng:"fever",vie :"Sốt"},
        {status: shortnessOfBreath, setStatus: setShortnessOfBreath,eng:"shortnessOfBreath",vie :"Khó thở"},
        {status: runningNose, setStatus: setRunningNose,eng:"runningNose",vie :"Chảy nước mũi"},
        {status: tiredness, setStatus: setTiredness,eng:"tiredness",vie :"Mệt mỏi"},
    ]

    const listCity = ["Quảng Bình","Quảng Trị","Quảng Đà Nẵng",]
    function formattedDate(date){
        let year = date.getFullYear();
        let month = date.getMonth()+1;
        if (month<10)
            month= `0${month}`;
        let day = date.getDate();
        if (day<10)
            day= `0${day}`;
        return `${year}-${month}-${day}`
    }


    const myDate = time2.convertDateToDate(date);
    const myTime = time2.convertDateToTime(time);

    useEffect(()=>{
        AsyncStorage.read()
            .then(token=>{
                Promise.all( [testingApi.getLocations(token),cityApi.getCities(token)])
                .then(res=>{

                    const {data} = res[0];
                    setData(data);
                    // console.log(999,res[1].data);
                    // console.log(111,data);
                    setCities(res[1].data);
                })
                .catch(er=>{
                    console.log(er);
                })
                .finally(()=>{

                })
            
            })
            .catch(err=>{
                console.log(err);
            })
            .finally(()=>{

            })
    },[])

    useEffect(()=>{
        setFilteredLocation(data.filter(value=>value.city.id==idCity))

        setIdLocation(0);
    },[idCity])

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(false);
        setDate(currentDate);
      };

    const onChangeTime = (event, selectedTime) => {
        const currentTime = selectedTime || time;
        setShowTime(false);
        setTime(currentTime);
      };



      const handleSubmit = ()=>{
        Promise.all([AsyncStorage.read(),AsyncStorage.read('id')])
            .then(values=>{
                const [token,accountId] = values;
                const params1 = {
                    testingLocationId : idLocation,
                    registerDate : new Date(Date.now()),
                    testingDate : new Date(formattedDate(date)+"T"+timeHour+":00:00"),
                }
                console.log("param1",params1);

                const param2 = {
                    accountId ,
                    asthma,
                    pregnancy,
                    highBloodPressure,
                    obesity,
                    heartProblem,
                    hiv,
                    cough,
                    fever,
                    shortnessOfBreath,
                    runningNose,
                    tiredness,
                    specialSymptom,

                }

                testingApi.register(token,params1)
                    .then(res=>{
                        const {data} = res;
                        console.log(res);
                        if (!data)
                            Alert.alert("Vui lòng chọn ngày khác");
                        else {
                            Toast.show("Đăng kí xét nghiệm thành công..");
                        }
                    })  
                    .catch(err=>{
                        console.log(err);
                    })

                //post medical
                userApi.medicalInfo(param2,token)
                    .then(res=>{
                        console.log("MEDICAL:",res);
                    })
                    .catch(err=>{
                        console.log(res);
                    })
            })

            .catch(err=>{
                console.log(err);
            })
 

      }



    return (
        <ScrollView  >
        <View style = {styles.container}>
            <Text style = {styles.header}> Đăng kí xét nghiệm</Text>


            <View style = {styles.row}>
                <Text style={styles.title}>Tỉnh/Thành phố: </Text>
                
                <Picker
                    style = {styles.location} 
                    selectedValue={idCity}
                    mode = {'dialog'}
                    onValueChange={(itemValue, itemIndex) =>
                        setIdCity(itemValue)
                    }>
                    {cities.map((value,index)=>(
                        <Picker.Item style = {styles.selectedDate} label ={value.name} value = {value.id} key={value.id}></Picker.Item>
                    ))}
                </Picker>
            </View>

            <View style = {styles.row}>
                <Text style={styles.title}>Nơi xét nghiệm: </Text>
                
                <Picker
                    style = {styles.location} 
                    selectedValue={idLocation}
                    mode = {'dialog'}
                    onValueChange={(itemValue, itemIndex) =>
                        setIdLocation(itemValue)
                    }>
                    {filteredLocation.map((value,index)=>(
                        <Picker.Item style = {styles.selectedDate} label ={value.name} value = {value.id} key={value.id}></Picker.Item>
                    ))}
                </Picker>
            </View>

            <View style= {styles.row}>
                <Text style={styles.title}>Ngày: </Text>
                {show && 
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={date}
                        mode={mode}
                        is24Hour={true}
                        minimumDate = {new Date(Date.now())}
                        onChange={onChange}
                    />
                }

                

                    
                <TouchableOpacity 
                    onPress = {()=>{
                        setShow(true);
                    }}
                >
                    <View style = {{flexDirection : 'row',alignItems : 'center',...styles.selectedDate}}>
                        <Text  style= {styles.datetime}>
                            {myDate[1]}
                        </Text>
                        <Icon name="calendar" size={30} color="#34c2ed" style = {{paddingLeft : 20}} />
                    </View>
                </TouchableOpacity>

            </View>

            <View style = {styles.row}>
                <Text style={styles.title}>Thời gian: </Text>

                <Picker
                    style = {styles.location} 
                    selectedValue={timeHour}
                    mode = {'dialog'}
                    onValueChange={(itemValue) =>
                        setTimeHour(itemValue)
                    }>
                    {listTime.map((value,i)=>(
                        <Picker.Item style = {styles.selectedDate} label ={value<10? "0"+value+" H" : value+" H"} value = {value} key={value.i}></Picker.Item>
                    ))}
                </Picker>

                {/* <TouchableOpacity 
                    onPress = {()=>{
                        setShowTime(true);
                    }}
                >
                    <View style = {{flexDirection : 'row',alignItems : 'center',...styles.selectedDate}}>
                        <Text  style= {styles.datetime}>
                            {formattedTime(time)}
                        </Text>
                        <Icon name="alarm" size={30} color="black" style = {{paddingLeft : 20}} />
                    </View>
                </TouchableOpacity> */}
            </View>


            


            <Text style = {styles.title}>Tình trạng sức khỏe:</Text>
            <View style = {{backgroundColor : "rgba(145, 141, 141,0.05)",marginVertical : 10,paddingVertical : 10,marginHorizontal : 10}}>
                <FlatList
                    nestedScrollEnabled = {true}
                    data = {listSymptoms}
                    renderItem = { ({item}) =>(
                        <Switch
                            item = {item}
                        />
                    )
                    }
                    numColumns = {2}
                    keyExtractor = {(listSymptoms,index)=>{
                        return index+"";
                    }}
                />
                
            </View>
            <TextInput style = {styles.specialSym}
                    onChangeText = {setSpecialSymptom}
                    placeholder = "Nhập triệu chứng của bạn..."
                ></TextInput>

            <Button 
                titleStyle = {styles.submitButton}
                title = {"Đăng kí"}
                onPress = {handleSubmit}
            ></Button>

        </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : 'white',
        paddingHorizontal :10,
        paddingVertical : 30,
    },
    
    header : {
        fontSize : 30,
        fontWeight : 'bold',
        textAlign : 'center',
    },

    title : {
        // color : 'white',
        color : 'black',
        fontWeight : 'bold',
        fontSize : 18,
        minWidth : 150,
    },

    button : {
        fontSize : 20,
        padding : 10,
        backgroundColor : '#0384fc',
        alignItems : 'center',
        width : 200,
    },

    selectedDate : {
        paddingVertical : 10,
        paddingHorizontal : 10,
        alignSelf : 'stretch',
        fontWeight : 'bold',
    },

    datetime : {
        fontSize : 20,

    },

    location : {
        flex : 1,
        fontWeight : 'bold',
        color : 'black',
    },

    specialSym: {
        marginVertical : 10,
        paddingHorizontal : 30,
        marginHorizontal : 30,
        borderWidth : 1,
        borderRadius : 20,

        alignContent : 'center',

    },

    row : { 
        display : 'flex',
        flexDirection : 'row',
        alignItems  : 'center',
        marginVertical : 10,
        // backgroundColor : "rgba(145, 141, 141,0.1)",
        borderWidth : 0.5,
        borderRadius : 20,
        paddingHorizontal : 20,
    },

    submitButton: {
        marginVertical : 10,
        ..._styles.colorButton,
        paddingHorizontal : 40,
    }

})