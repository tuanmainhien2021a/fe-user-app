import React, { useCallback, useEffect, useState } from 'react';
import { RefreshControl, StyleSheet, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import Spinner from 'react-native-loading-spinner-overlay';
import userApi from '../../api/userApi';
import TestingResult from '../../components/TestingResult';
import AsyncStorage from '../../storage';



export default function (props) {
    const {navigation} = props; 
    const [data,setData] = useState([]);
    const [update,setUpdate] = useState(0);
    const [mustUpdate,setMustUpdate] = useState(false);
    const [isLoading,setIsLoading] = useState(true);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(()=>{
        AsyncStorage.read()
            .then(token=>{
                return userApi.testingResult(token)
            .then(res=>{
                const {data} = res
                setData(data);
            })
                
            })
            .catch(err=>{
                console.log(err);
            })
            .finally(
                ()=>{
                    setIsLoading(false)
                }
            )
    },[mustUpdate])

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', e => {
          // Prevent default behavior
          setUpdate(update+1);
          AsyncStorage.read()
            .then(token=>{
                return userApi.testingResult(token)
            .then(res=>{
                const {data} = res
                setData(data);
                console.log("DATA",data);
            })
                
            })
            .catch(err=>{
                console.log(err);
            })
            .finally(
                ()=>{
                    setIsLoading(false)
                }
            )
          
          // ...
        });
      
        return unsubscribe;
      }, []);

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const onRefresh = useCallback(() => {
        setRefreshing(true);
        wait(500).then(() => setRefreshing(false));
        setMustUpdate(!mustUpdate)
    }, [mustUpdate]);
    
    return (
        <ScrollView style ={styles.container}
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        >
            <Spinner
            visible={isLoading}
            />
            <View >
                <Text style = {styles.header}> Thông tin xét nghiệm</Text>

                <View style={styles.infoContainer}>
                    {/* <FlatList
                        data = {data}
                        renderItem = {({item})=>(
                            <TestingResult
                                item = {item}
                            />
                        )}
                        keyExtractor ={(item,index)=> index+""}
                    /> */}
                    {data.map(item=>(
                        <TestingResult
                            item = {item}
                        />
                    ))}
                </View>

                </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        backgroundColor : 'white',
        paddingHorizontal : 10,
    },

    header : {
        fontWeight : 'bold',
        fontSize : 30,
        textAlign : 'center',
        paddingTop : 30,
        // backgroundColor : 'pink'
    },

    infoContainer : {
        paddingTop : 30,

    },

    dataContainer : {
        display : 'flex',
        flexDirection : 'row',
    },

    title : {
        fontSize : 25,  
        fontWeight : 'bold',
    },

    result :{
    }
})