import React from 'react';
import { Image, ScrollView, StyleSheet, Text } from 'react-native';
import HTML from "react-native-render-html";

export default function (props){
    const item = props.route.params;
    // const tab = "\t\t\t\t"
    // item.content = item.content.replaceAll("\n","\n"+tab);
    // item.content = tab + item.content;
    
    return (
        <ScrollView style={styles.container}>
            <Text style ={styles.title}>
                {item.title}
            </Text>
            <Image 
                style = {styles.categoryImage}
                source={    {uri: item.image}}

            />
            {/* <Text style={styles.content}>
                {item.content}
            </Text> */}
            <HTML baseFontStyle = {{ fontSize: 20 }}
                source = {{html : item.content}}
            />
        </ScrollView>
    )


}

const styles = StyleSheet.create({
    container:{
        flex : 1,
        // paddingHorizontal : 10,
        padding : 10,
        backgroundColor : 'white',
    },

    content : {
        fontSize : 20, 
        paddingHorizontal : 20,

    },

    title : {
        color : 'black',
        fontSize : 35,
        alignItems : 'flex-start',
        fontWeight : 'bold',
    },

    categoryImage : {
        alignSelf : 'stretch',
        height : 250,
    },

})
